import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum AnotherStatus {

  ONLINE("ONLINE", 1),
  CHATTING("CHATTING", 2),
  ON_COUNSEL("ON_COUNSEL", 2),
  ONLY("ONLY", 3),
  OFFLINE("OFFLINE", 3),
  NO_STATUS("NO_STATUS", 4);


  @Getter
  private final String code;

  @Getter
  private final int priority;

}
