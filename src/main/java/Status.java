import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum Status {

  ONLINE("ONLINE", "ONLINE"), OFFLINE("OFFLINE", "OFFLINE"), CHATTING("CHATTING", "CHATTING");

  @Getter
  private final String code;

  @Getter
  private final String label;

}
