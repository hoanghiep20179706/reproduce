import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@NoArgsConstructor
@Accessors(chain = true)
public class BasicResponse {

  private static final long serialVersionUid = -6527546854906594L;

  private long id;
  private String name;
  private String imageUrl;
  private Double rating;
  private ExpertResponseStatus status;
  private Status originalStatus;

  private boolean isNew;

  public String toString() {
    return "Status = " + status + ", id = " + id;
  }
}

