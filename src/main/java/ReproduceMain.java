import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.LongStream;

public class ReproduceMain {

  private static final Comparator<ServerResponse> tellerComparator = (o1, o2) -> {
    if (!Objects.equals(o1.getStatus(), o2.getStatus())) {
      final AnotherStatus o1Status = AnotherStatus.valueOf(
          o1.getStatus() != null ? o1.getStatus().name() : "NO_STATUS"), o2Status =
          AnotherStatus.valueOf(
              o2.getStatus() != null ? o2.getStatus().name() : "NO_STATUS");
      return Long.compare(o2Status.getPriority(), o1Status.getPriority());
    }

    if (!Objects.equals(o1.getReview(), o2.getReview())) {
      return Long.compare(o2.getReview(), o1.getReview());
    }

    return Long.compare(o2.getId(), o1.getId());
  };

  public static void main(String[] args) {

    ServerResponse responseViolation = new ServerResponse();
    responseViolation.setStatus(ExpertResponseStatus.ONLY);
    responseViolation.setPrice(6969);
    responseViolation.setId(8L);
    responseViolation.setReview(9L);

    List<ServerResponse> responses = new ArrayList<>();

    AtomicInteger idIncrement = new AtomicInteger(0);

    Arrays.asList(ExpertResponseStatus.OFFLINE, ExpertResponseStatus.ONLY)
        .forEach(expertResponseStatus -> IntStream.range(0, 10).boxed().forEach(id -> LongStream.range(0, 10).boxed().forEach(review -> {
          idIncrement.incrementAndGet();
          ServerResponse serverResponse = new ServerResponse();
          serverResponse.setId(id);
          serverResponse.setReview(review);
          serverResponse.setStatus(expertResponseStatus);
          responses.add(serverResponse);
          if (idIncrement.get() == 31) {
            responses.add(responseViolation);
          }
        })));

    try {
      responses.stream().sorted(tellerComparator).collect(Collectors.toList()).forEach(
          System.out::println);
    } catch (Exception ex) {
      ex.printStackTrace();
    }
  }
}

