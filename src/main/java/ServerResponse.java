import java.util.List;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@NoArgsConstructor
@Accessors(chain = true)
@ToString(callSuper = true)
public class ServerResponse extends BasicResponse {

  private static final long serialVersionUid = -6527546854906594L;

  private long review;
  private Integer price;
  private List<String> specialities;
  private List<String> categories;
  private Status originalStatus;

  private boolean isNew;
}

